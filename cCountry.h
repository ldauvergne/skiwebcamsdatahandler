#ifndef CCOUNTRY_H
#define CCOUNTRY_H
#include "cResort.h"

#include "lib/tinyxml.h"
#include <iostream>
#include <string>
#include <deque>

using namespace std;

class cCountry
{
public:
    cCountry(TiXmlElement* pCountryData);
    ~cCountry();

    const char* mGetCountryName();
    cCountry mLoadCountry();
    int mGetResortsNumber();
    cResort* mGetResort(int pIndex);
     void mSetResort(int pIndex,cResort pResort);
protected:
private:

    deque<cResort*>  aResorts;
    const char* aName;
    TiXmlElement* aCountryData;
};

#endif // CCOUNTRY_H
