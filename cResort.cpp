#include "cResort.h"


cResort::cResort(TiXmlElement* pResortData)
{
    aName=pResortData->FirstChildElement()->GetText();

    aWebcamsData=pResortData->FirstChildElement()->NextSiblingElement();
}

void cResort::mLoadWebcams()
{
    for(TiXmlElement* lElem = aWebcamsData; lElem != NULL; lElem = lElem->NextSiblingElement())
    {
        if (lElem->GetText()!=NULL){
        aWebcams.push_back((char*)lElem->GetText());
        }
    }
}


list<char*> cResort::mGetResortWebcams()
{
    return aWebcams;
}

const char* cResort::mGetResortName()
{
    return aName;
}

cResort::~cResort()
{

}
