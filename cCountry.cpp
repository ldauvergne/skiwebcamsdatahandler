#include "cCountry.h"

cCountry::cCountry(TiXmlElement* pCountryData)
{
    aName=pCountryData->FirstChildElement()->GetText();
    aCountryData=pCountryData->FirstChildElement()->NextSiblingElement();
}

cCountry cCountry::mLoadCountry()
{
    cResort* lResort = NULL;
    for(TiXmlElement* lElem = aCountryData; lElem != NULL; lElem = lElem->NextSiblingElement())
    {

        lResort = new cResort(lElem);
        aResorts.push_back(lResort);

    }

    return *this;
}

cResort* cCountry::mGetResort(int pIndex)
{
    return aResorts.at(pIndex);
}
void cCountry::mSetResort(int pIndex,cResort pResort)
{
    *aResorts[pIndex]=pResort;
}

int cCountry::mGetResortsNumber()
{
    return aResorts.size();
}

const char* cCountry::mGetCountryName()
{
    return aName;
}

cCountry::~cCountry()
{

}
