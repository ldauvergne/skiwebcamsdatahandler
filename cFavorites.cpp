#include "cFavorites.h"

cFavorites::cFavorites(string pWorldDate, const char* pFavsFilePath)
{
    cout << "Favorites creation" << endl;
    aWorldDate = pWorldDate;
    mInitFavorites(pFavsFilePath);
    cout << "Favorites created" << endl;
}

void cFavorites::mAddFavorite(int pCountry, int pResort)
{
    lFavorites.push_back(make_pair(pCountry, pResort));

}
;

void cFavorites::mDeleteFavorite(int pIndex)
{
    lFavorites.erase(lFavorites.begin() + pIndex);
    mSaveFavoritesToFile();
}
;

void cFavorites::mInitFavorites(const char* pFilePath)
{
    cout << "Favorites init creation" << endl;
    aFavorite_File_Path = pFilePath;

    aFavsDoc = new TiXmlDocument();
    if (!aFavsDoc->LoadFile(aFavorite_File_Path)) {
        //cout << aFavsDoc->ErrorDesc() << endl;
        cout << "Favorites file not load, reset" << endl;
        aFavsDoc = new TiXmlDocument();
        aFavsDocDate = aWorldDate;
    } else {
        cout << "Loading favorites" << endl;
        aFavsDocDate = aFavsDoc->FirstChildElement()->FirstChildElement()->GetText();
        if (aFavsDocDate.compare(aWorldDate) != 0) {
            cout << aFavsDocDate << endl;
            cout << aWorldDate << endl;
            aFavsDoc = new TiXmlDocument();
            aFavsDocDate = aWorldDate;
            cout << "Favorites date false, reset" << endl;
        } else {
            stringstream ss;
            for (TiXmlElement* lElem =
                    aFavsDoc->FirstChildElement()->FirstChildElement()->NextSiblingElement();
                    lElem != NULL; lElem = lElem->NextSiblingElement()) {
                ss << lElem->FirstChildElement()->GetText();
                int CountryIndex;
                ss >> CountryIndex;
                ss.clear();
                ss.str("");

                ss << lElem->FirstChildElement()->NextSiblingElement()->GetText();
                int ResortIndex;
                ss >> ResortIndex;
                ss.clear();
                ss.str("");

                mAddFavorite(CountryIndex, ResortIndex);
            }
        }
    }
    cout << "Favorites init done" << endl;
}

void cFavorites::mSaveFavoritesToFile()
{
    aFavsDoc = new TiXmlDocument();
    TiXmlElement* lFavoritesDoc = new TiXmlElement("FavoritesDoc");

    aFavsDocDate = aWorldDate;
    TiXmlElement* lFavsDate = new TiXmlElement("Favorites_Date");
    lFavsDate->LinkEndChild(new TiXmlText(aFavsDocDate.c_str()));
    lFavoritesDoc->LinkEndChild(lFavsDate);

    stringstream ss; //create a stringstream

    cout << "Saving favorites" << endl;

    for (unsigned int i = 0; i < lFavorites.size(); i++) {

        TiXmlElement* lResort = new TiXmlElement("Resort");

        ss << lFavorites.at(i).first; //add number to the stream

        TiXmlElement* CountryIndex = new TiXmlElement("CountryIndex");
        CountryIndex->LinkEndChild(new TiXmlText((ss.str()).c_str()));
        lResort->LinkEndChild(CountryIndex);
        ss.clear();
        ss.str("");

        ss << lFavorites.at(i).second; //add number to the stream

        TiXmlElement* ResortIndex = new TiXmlElement("ResortIndex");
        ResortIndex->LinkEndChild(new TiXmlText((ss.str()).c_str()));
        lResort->LinkEndChild(ResortIndex);
        ss.clear();
        ss.str("");

        lFavoritesDoc->LinkEndChild(lResort);
    }
    cout << "End of saving favorites" << endl;

    aFavsDoc->LinkEndChild(lFavoritesDoc);
    aFavsDoc->SaveFile(aFavorite_File_Path);
    aFavsDoc->Clear();
}
;

cFavorites::~cFavorites()
{
}
