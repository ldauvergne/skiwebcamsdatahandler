#ifndef CFAVORITES_H
#define CFAVORITES_H

#include "lib/tinyxml.h"
#include "lib/tinyxml.h"
#include <iostream>
#include <vector>
#include <utility>
#include <sstream>

using namespace std;

class cFavorites
{
    public:
        cFavorites(string pWorldDate,const char* pFavsFilePath);
        vector<pair<int,int> > lFavorites;
        void mAddFavorite(int pCountry, int pResort);
        void mDeleteFavorite(int pIndex);
        void mSaveFavoritesToFile();
        void mInitFavorites(const char* pFilePath);
        virtual ~cFavorites();
    protected:
    private:
        const char* aFavorite_File_Path;
        string aFavsDocDate;
        string aWorldDate;
        TiXmlDocument* aFavsDoc;
};

#endif // CFAVORITES_H
