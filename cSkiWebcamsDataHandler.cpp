#include "cSkiWebcamsDataHandler.h"

cSkiWebcamsDataHandler::cSkiWebcamsDataHandler(const char* pFilePath, const char* pFavsFilePath)
{
    cout << "Data Handler creation." << endl;
    aDoc = new TiXmlDocument();
    if (!aDoc->LoadFile(pFilePath)) {
        cerr << aDoc->ErrorDesc() << endl;
    }
    aWorld = new cWorld(aDoc->FirstChildElement(),pFavsFilePath);
    cout << "Data Handler created." << endl;

}

void cSkiWebcamsDataHandler::mLoadAll()
{
    for (int lIndex = 0; lIndex != mGetCountriesNumber(); lIndex++) {

        mLoadCountry(lIndex);

        for (int lIndex2 = 0; lIndex2 != mGetResortsNumber(lIndex); lIndex2++) {
            mLoadResort(lIndex, lIndex2);

            list<char*> lWebcamsList =
                    aWorld->mGetCountry(lIndex)->mGetResort(lIndex2)->mGetResortWebcams();
        }
    }
}

const char * cSkiWebcamsDataHandler::mGetDate(){
    return aWorld->mGetDate().c_str();
}

const char * cSkiWebcamsDataHandler::mGetTitre(){
    return aWorld->mGetTitre().c_str();
}

void cSkiWebcamsDataHandler::mLoadCountry(int pCountryIndex)
{
    aWorld->mGetCountry(pCountryIndex)->mLoadCountry();
}

void cSkiWebcamsDataHandler::mLoadResort(int pCountryIndex, int pResortIndex)
{
    aWorld->mGetCountry(pCountryIndex)->mGetResort(pResortIndex)->mLoadWebcams();
}

int cSkiWebcamsDataHandler::mGetCountriesNumber()
{
    return aWorld->mGetCountriesNumber();
}

int cSkiWebcamsDataHandler::mGetResortsNumber(int pCountryIndex)
{
    return aWorld->mGetCountry(pCountryIndex)->mGetResortsNumber();
}

int cSkiWebcamsDataHandler::mGetWebcamsNumber(int pCountryIndex, int pResortIndex)
{
    return aWorld->mGetCountry(pCountryIndex)->mGetResort(pResortIndex)->mGetResortWebcams().size();
}

list<const char*> cSkiWebcamsDataHandler::mGetCountriesName()
{
    list<const char*> lCountriesNamesList;

    for (int lIndex = 0; lIndex != mGetCountriesNumber(); lIndex++) {
        lCountriesNamesList.push_back(aWorld->mGetCountry(lIndex)->mGetCountryName());

    }

    return lCountriesNamesList;
}

list<const char*> cSkiWebcamsDataHandler::mGetResortsName(int pCountryIndex)
{
    list<const char*> lResortsNamesList;

    for (int lIndex = 0; lIndex != mGetResortsNumber(pCountryIndex); lIndex++) {
        lResortsNamesList.push_back(mGetResortName(pCountryIndex, lIndex));
    }
    return lResortsNamesList;
}

list<const char*> cSkiWebcamsDataHandler::mGetFavoritesName()
{
    list<const char*> lFavoritesNamesList;

    for (unsigned int lIndex = 0; lIndex != aWorld->aFavorites->lFavorites.size(); lIndex++) {

        lFavoritesNamesList.push_back(mGetResortName(aWorld->aFavorites->lFavorites.at(lIndex).first, aWorld->aFavorites->lFavorites.at(lIndex).second));
    }
    return lFavoritesNamesList;
}

list<char*> cSkiWebcamsDataHandler::mGetFavoriteUrl(int pIndex)
{
    return mGetWebcams(aWorld->aFavorites->lFavorites.at(pIndex).first, aWorld->aFavorites->lFavorites.at(pIndex).second);
}

void cSkiWebcamsDataHandler::mAddFavorite(int pCountryIndex, int pResortIndex)
{
    aWorld->aFavorites->mAddFavorite(pCountryIndex, pResortIndex);
    aWorld->aFavorites->mSaveFavoritesToFile();

}

void cSkiWebcamsDataHandler::mDeleteFavorite(int pIndex)
{
    aWorld->aFavorites->mDeleteFavorite(pIndex);
}

const char* cSkiWebcamsDataHandler::mGetResortName(int pCountryIndex, int pResortIndex)
{
    return aWorld->mGetCountry(pCountryIndex)->mGetResort(pResortIndex)->mGetResortName();
}

list<char*> cSkiWebcamsDataHandler::mGetWebcams(int pCountryIndex, int pResortIndex)
{

    return aWorld->mGetCountry(pCountryIndex)->mGetResort(pResortIndex)->mGetResortWebcams();
}

cSkiWebcamsDataHandler::~cSkiWebcamsDataHandler()
{
}
