#include "cWorld.h"


cWorld::cWorld(TiXmlElement* pWorldData, const char* pFavsFilePath)
{
    cout << "World creation" << endl;
    aWorldData = mLoadGeneralData(pWorldData);

    mLoadCountries(aWorldData);
    cout << aTitre << endl;
    cout << aDate << endl;
    cout << "Number of countries : " << aCountries.size()<< endl;

    aFavorites = new cFavorites(aDate,pFavsFilePath);
    cout << "World created" << endl;
}

TiXmlElement* cWorld::mLoadGeneralData(TiXmlElement* pWorldData)
{
    this->aTitre=pWorldData->FirstChildElement()->GetText();
    this->aDate=pWorldData->FirstChildElement()->NextSiblingElement()->GetText();

    return pWorldData->FirstChildElement()->NextSiblingElement()->NextSiblingElement();
}

void cWorld::mLoadCountries(TiXmlElement* pWorldData)
{
    for(TiXmlElement* lElem = pWorldData; lElem != NULL; lElem = lElem->NextSiblingElement())
    {
        cCountry *lCountry=new cCountry(lElem);
        aCountries.push_back(lCountry);
    }
}

int cWorld::mGetCountriesNumber()
{
    return aCountries.size();
}

cCountry* cWorld::mGetCountry(int pIndex)
{
    return aCountries.at(pIndex);
}

void cWorld::mSetCountry(int pIndex,cCountry pCountry)
{
    *aCountries[pIndex]=pCountry;
}

cWorld::~cWorld()
{

}
