#ifndef CSKIWEBCAMSDATAHANDLER_H
#define CSKIWEBCAMSDATAHANDLER_H

#include "lib/tinyxml.h"
#include "cWorld.h"
#include "cFavorites.h"
#include <iostream>
#include <string>

using namespace std;

class cSkiWebcamsDataHandler
{
public:
    cSkiWebcamsDataHandler(const char* pFilePath,const char* pFavsFilePath);


    void mLoadAll();
    void mLoadCountry(int pCountryIndex);
    void mLoadResort(int pCountryIndex,int pResortIndex);

    const char * mGetDate();
    const char * mGetTitre();
    list<const char*> mGetCountriesName();
    list<const char*> mGetResortsName(int pCountryIndex);
    list<const char*> mGetFavoritesName();
    void mAddFavorite(int pCountryIndex, int pResortIndex);
    void mDeleteFavorite(int pIndex);
    list<char*> mGetFavoriteUrl(int pIndex);
    const char* mGetResortName(int pCountryIndex,int pResortIndex);
    list<char*> mGetWebcams(int pCountryIndex,int pResortIndex);

    ~cSkiWebcamsDataHandler();
protected:
private:
    cWorld* aWorld;
    TiXmlDocument* aDoc;

    int mGetCountriesNumber();
    int mGetResortsNumber(int pCountryIndex);
    int mGetWebcamsNumber(int pCountryIndex,int pResortIndex);
};

#endif // CSKIWEBCAMSDATAHANDLER_H
