#ifndef CWORLD_H
#define CWORLD_H
#include "cCountry.h"
#include "cFavorites.h"

#include "lib/tinyxml.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class cWorld
{
public:
    cWorld(TiXmlElement* pWorldData, const char* pFavsFilePath);
    ~cWorld();

    int mGetCountriesNumber();
    cFavorites* aFavorites;

    string mGetDate(){return aDate;};
    string mGetTitre(){return aTitre;};
    cCountry* mGetCountry(int pIndex);

    void mSetCountry(int pIndex,cCountry pCountry);

protected:
private:
    deque<cCountry*> aCountries;
    string aDate;
    string aTitre;
    TiXmlElement* aWorldData;


    TiXmlElement* mLoadGeneralData(TiXmlElement* pWorldData);
    void mLoadCountries(TiXmlElement* pWorldData);
};

#endif // CWORLD_H
