#ifndef CRESORT_H
#define CRESORT_H

#include "lib/tinyxml.h"
#include <iostream>
#include <string>
#include <list>

using namespace std;

class cResort
{
public:
    cResort(TiXmlElement* pResortData);
    list<char*> mGetResortWebcams();
    const char* mGetResortName();
    void mLoadWebcams();
    virtual ~cResort();
protected:
private:
    list<char*> aWebcams;
    const char* aName;
    TiXmlElement* aWebcamsData;
};

#endif // CRESORT_H
